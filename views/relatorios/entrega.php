<h3 class='page-title'>Relatório de Entrega</h3>
<p>Esse relatório exibe quantas marmitas devem ser entregues a cada cliente, separadas por etiquetas. Exemplo: 35 marmitas "A" para o cliente "Da Silva".</p>
<p>Como é tudo pré-estabelecido no momento do contrato do cliente e as quantidades são definidas no processo de pedidos, basta o entregador seguir esse relatório para entregar as marmitas certas, em quantidade certas nos clientes determinados.</p>

<b>Dados: </b>
<pre>
    <?php print_r($dados);?>
</pre>