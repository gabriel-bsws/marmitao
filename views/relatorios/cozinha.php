<h3 class='page-title'>Relatório de Cozinha</h3>
<p>Esse relatório exibe as quantidades de marmitas a serem produzidas no dia, divididas por composição de ingredientes. Ou seja: quantas marmitas de cada composição a cozinha deve produzir.</p>
<p>O sistema atribui uma etiqueta para cada composição de marmita.</p>
<p>Tendo em vista que o sistema sabe, de maneira automática, qual marmita produzir para cada cliente, de acordo com o dia da semana, e o usuário pode apenas definir a quantidade, isso evita o erro de produzir as marmitas em quantidades e composições erradas.</p>

<b>Dados: </b>
<pre>
    <?php print_r($dados);?>
</pre>