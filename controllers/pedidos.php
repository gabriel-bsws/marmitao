<?php

class Pedidos extends Controller {

    private $model;
    private $module;

    public function __construct() {
        global $system;
        $system->auth();

        $this->module = "pedidos";

        require_once $_SERVER['DOCUMENT_ROOT'] . "/models/" . $this->module . ".php";

        $classname = "Model" . ucfirst($this->module);
        $this->model = new $classname();
    }

    public function lista() {
        $list = $this->model->_list();

        self::view("common", "top");
        self::view($this->module, "list", array("list" => $list));
        self::view("common", "footer");
    }

    public function register() {
        global $system;

        $dia = date("w", mktime());

        $clients = $this->model->clientes_dia($dia);

        $beginOfDay = strtotime("midnight", mktime());

        $test = $this->model->test_day($beginOfDay);

        if (!empty($test)) {
            $system->alert("Os pedidos de hoje já foram fechados.");
            header("Location: /");
        } else {
            self::view("common", "top");
            self::view($this->module, "register", array("dia" => $dia, "clients" => $clients));
            self::view("common", "footer");
        }
    }

    public function salvar() {
        global $system;

        $data = $_POST['pedidos'];

        if ($this->model->_save($data)) {
            header("Location: /?c=relatorios&a=cozinha");
        } else {
            $system->alert("Ocorreu uma falha. As infomações não foram salvas.", ALERT_ERROR);
            header("Location: " . $_SERVER['HTTP_REFERER']);
        }
    }

    public function delete() {
        global $system;

        if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if ($this->model->_delete($id)) {
                $system->alert("O registro foi excluído.", ALERT_SUCCESS);
            } else {
                $system->alert("Ocorreu uma falha inesperada. O registro não pôde ser excluído.", ALERT_ERROR);
            }
        } else {
            $system->alert("O sistema não pôde identificar o registro a ser excluído.", ALERT_ERROR);
        }

        header("Location: /?c=" . $this->module . "&a=lista");
    }

}

?>