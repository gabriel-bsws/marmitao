<?php

class Marmitas extends Controller {

    private $model;
    private $module;

    public function __construct() {
        global $system;
        $system->auth();

        $this->module = "marmitas";

        require_once $_SERVER['DOCUMENT_ROOT'] . "/models/" . $this->module . ".php";

        $classname = "Model" . ucfirst($this->module);
        $this->model = new $classname();
    }

    public function lista($id_cliente) {
        return $this->model->_list($id_cliente);
    }

    public function register() {
        global $system;

        $ingr = $system->execute("ingredientes", "lista", false);

        self::view($this->module, "register", array("ingr" => $ingr, "c_id" => $_REQUEST['c_id'], "c_nome" => $_REQUEST['cliente']));
    }

    public function salvar() {
        global $system;

        $marm_data = array(
            "custo" => $_POST["custo"],
            "dia" => $_POST["dia"],
            "id_cliente" => $_POST["id_cliente"]
        );

        if (empty($_POST["ingredients"])) {
            $system->alert("Você precisa selecionar ao menos um ingrediente para a marmita.", ALERT_ERROR);
        } else {
            $assoc_data = $_POST["ingredients"];
            if ($last = $this->model->_save((object) $marm_data)) {
                if ($this->model->save_assoc($assoc_data, $last)) {
                    $system->alert("As informações foram salvas com sucesso.", ALERT_SUCCESS);
                } else {
                    $this->model->_delete($last);
                    $system->alert("Ocorreu uma falha. As infomações não foram salvas.", ALERT_ERROR);
                }
            }
        }

        header("Location: /?c=cliente&a=register&id=" . $marm_data['id_cliente']);
    }

    public function delete() {
        global $system;

        if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if ($this->model->_delete($id) && $this->model->del_assoc($id)) {
                $system->alert("O registro foi excluído.", ALERT_SUCCESS);
            } else {
                $system->alert("Ocorreu uma falha inesperada. O registro não pôde ser excluído.", ALERT_ERROR);
            }
        } else {
            $system->alert("O sistema não pôde identificar o registro a ser excluído.", ALERT_ERROR);
        }

        header("Location: /?c=cliente&a=register&id=".$_REQUEST['id_cliente']);
    }
    
    public function info(){
        $m_id = $_REQUEST['id'];
        
        $cliente = $_REQUEST['cliente'];
        
        $m_data = $this->model->_get($m_id);
        $ingrs = $this->model->get_ingr($m_id);
        
        self::view("marmitas", "info", array("m_data" => $m_data, "ingrs" => $ingrs,"cliente"=>$cliente));
    }

}

?>