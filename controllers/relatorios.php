<?php

class Relatorios extends Controller {

    private $model;
    private $module;

    public function __construct() {
        global $system;
        $system->auth();

        $this->module = "relatorios";

        require_once $_SERVER['DOCUMENT_ROOT'] . "/models/" . $this->module . ".php";

        $classname = "Model" . ucfirst($this->module);
        $this->model = new $classname();
    }
    
    public function cozinha(){
        $beginOfDay = strtotime("midnight", mktime());
        $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
        
        self::view("common", "top");
        self::view("relatorios", "cozinha", array("dados" => $this->model->_list()));
        self::view("common", "footer");
    }
    
    public function entrega(){
        self::view("common", "top");
        self::view("relatorios", "entrega", array("dados" => $this->model->_list()));
        self::view("common", "footer");
    }
    
    public function custos(){
        self::view("common", "top");
        self::view("relatorios", "custos", array("dados" => $this->model->_list()));
        self::view("common", "footer");
    }

}

?>