<?php

class Ingredientes extends Controller {

    private $model;
    private $module;

    public function __construct() {
        global $system;
        $system->auth();

        $this->module = "ingredientes";

        require_once $_SERVER['DOCUMENT_ROOT'] . "/models/" . $this->module . ".php";

        $classname = "Model" . ucfirst($this->module);
        $this->model = new $classname();
    }

    public function lista($view = true) {
        $list = $this->model->_list();

        if ($view) {
            self::view("common", "top");
            self::view($this->module, "list", array("list" => $list));
            self::view("common", "footer");
        } else{
            return $list;
        }
    }

    public function register() {
        $_data = null;
        if (!empty($_REQUEST['id'])) {
            $_data = $this->model->_get($_REQUEST['id']);
        }
        
        self::view("common", "top");
        self::view($this->module, "register", array("data" => $_data[0]));
        self::view("common", "footer");
    }

    public function salvar() {
        global $system;

        if (empty($_POST['id']))
            unset($_POST['id']);

        if ($this->model->_save((object) $_POST)) {
            $system->alert("As informações foram salvas com sucesso.", ALERT_SUCCESS);
        } else {
            $system->alert("Ocorreu uma falha. As infomações não foram salvas.", ALERT_ERROR);
        }

        header("Location: /?c=" . $this->module . "&a=lista&args=true");
    }

    public function delete() {
        global $system;

        if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if ($this->model->_delete($id) && $this->model->del_assoc($id)) {
                $system->alert("O registro foi excluído.", ALERT_SUCCESS);
            } else {
                $system->alert("Ocorreu uma falha inesperada. O registro não pôde ser excluído.", ALERT_ERROR);
            }
        } else {
            $system->alert("O sistema não pôde identificar o registro a ser excluído.", ALERT_ERROR);
        }

        header("Location: /?c=" . $this->module . "&a=lista&args=true");
    }

}

?>