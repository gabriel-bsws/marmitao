<?php

class Dbcom {

    private static function init() {
        
    }
    
    public static function insert($sql){
        //Desenvolver mecanismo para que a função só faça inserts.
        
        $cnn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

        if($cnn->query($sql)){
            $ret = $cnn->insert_id;
        } else{
            $ret = false;
        }
        
        $cnn->close();
        return $ret;
    }

    public static function query($sql) {
        $cnn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

        $res = $cnn->query($sql);

        if ($res === true || $res === false) {
            $ret = $res;
        } else {
            $ret = array();
            while ($row = $res->fetch_assoc()) {
                $ret[] = (object) $row;
            }
        }

        $cnn->close();
        return $ret;
    }

}

?>
